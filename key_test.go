package go_digital_signature

import (
	"crypto"
	"testing"
)

func Test_CreateKeyPair(t *testing.T) {
	t.Parallel()
	sk, pk, err := CreateKeyPair()
	t.Logf("%s, %s, %+v\n", string(sk), string(pk), err)
}

func Test_DecodePrivateKey_DecodePublicKey(t *testing.T) {
	t.Parallel()
	sk, pk, _ := CreateKeyPair()

	t.Log(DecodePrivateKey(sk))
	t.Log(DecodePublicKey(pk))
}

func Test_Sign_Verify(t *testing.T) {
	t.Parallel()
	skb, pkb, _ := CreateKeyPair()
	sk, _ := DecodePrivateKey(skb)
	pk, _ := DecodePublicKey(pkb)

	h := crypto.Hash.New(crypto.SHA256)
	h.Write([]byte("適当な文字列をハッシュ化"))
	hash := h.Sum(nil)

	sign, err := Sign(sk, hash)
	t.Log(sign, err)
	t.Log(Verify(pk, hash, sign))
}
