package go_digital_signature

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

func CreateKeyPair() ([]byte, []byte, error) {
	sk, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}

	skb := x509.MarshalPKCS1PrivateKey(sk)
	skp := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: skb,
	})

	pkb := x509.MarshalPKCS1PublicKey(&sk.PublicKey)
	pkp := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: pkb,
	})

	return skp, pkp, nil
}

func DecodePrivateKey(b []byte) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode(b)
	return x509.ParsePKCS1PrivateKey(block.Bytes)
}

func DecodePublicKey(b []byte) (*rsa.PublicKey, error) {
	block, _ := pem.Decode(b)
	return x509.ParsePKCS1PublicKey(block.Bytes)
}

func Sign(key *rsa.PrivateKey, hash []byte) ([]byte, error) {
	return rsa.SignPKCS1v15(rand.Reader, key, crypto.SHA256, hash)
}

func Verify(key *rsa.PublicKey, hash []byte, sign []byte) bool {
	return rsa.VerifyPKCS1v15(key, crypto.SHA256, hash, sign) == nil
}
